/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2017, ITU/ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

 /** \file     TAppMctsExtTop.h
  \brief    MCTS Extractor application class (header)
  */

#ifndef __TAPPMCTSEXTTOP__
#define __TAPPMCTSEXTTOP__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "TLibDecoder/TDecTop.h"
//#include "TLibEncoder/TEncTop.h"
#include "DecoderLib/DecLib.h"
#include "EncoderLib/EncLib.h"

//#if MCTS_EXTRACTION
//#include "TAppMctsExtCfg.h"
#include "MctsExtAppCfg.h"
  //! \ingroup TAppMctsExt
  //! \{

  // ====================================================================================================================
  // Class definition
  // ====================================================================================================================

  /// MCTS Extraction application class
class MctsExtApp : public MctsExtCfg
{
private:
  // class interface
  //TDecTop                         m_cTDecTop;                     ///< decoder class
  DecLib                          m_DecLib;                     ///< decoder class
  //TComSlice*                      m_pcSlice;                      ///< slice header class
  Slice*                          m_Slice;                      ///< slice header class
  //TDecEntropy                     m_cEntropyDecoder;              ///< entropy decoder class

  //TDecCavlc                       m_cCavlcDecoder;                ///< CAVLC decoder class
  CABACDecoder                    m_CabacDecoder;                 ///< CABAC decoder class
  //TEncEntropy                     m_cEntropyCoder;                ///< entropy encoder class    
  HLSWriter                       m_HLSWriter;                      ///< entropy encoder class (maybe, hope so)
  HLSyntaxReader                  m_HLSReader;                ///< entropy decoder class (maybe, hope so)
  //TEncCavlc                       m_cCavlcCoder;                  ///< CAVLC encoder class
  CABACEncoder                    m_CabacCoder;                   ///< CABAC encoder class
  CABACWriter*                    m_CABACWriter;
  //MctsExtractorTComPicSym         m_cSlicePicSym;

  uint32_t                        m_tileWidth;
  uint32_t                        m_tileHeight;
  uint32_t                        m_numTileCols;                       //!< number of tile columns
  uint32_t                        m_numTileRows;                       //!< number of tile rows
  uint32_t                        m_numNALSlice = 0;
  std::vector<uint32_t>           m_tileColWidth;                 //!< tile column widths in units of CTUs
  std::vector<uint32_t>           m_tileRowHeight;                //!< tile row heights in units of CTUs
  uint32_t                        m_picWidthInCtu = 0;                     //!< picture width in units of CTUs
  uint32_t                        m_picHeightInCtu = 0;                    //!< picture height in units of CTUs
  uint32_t                        m_CtuSize = 0;

  bool                            m_mctsExtractionInfoPresent;    ///< indicates whether MCTS extraction info for the traget mcts idx has been found in the bitstream
  bool                            m_VPSPresent;                   ///< indicates whether VPS has been found in the bitstream
  bool                            m_SPSPresent;                   ///< indicates whether SPS has been found in the bitstream
  bool                            m_PPSPresent;                   ///< indicates whether PPS has been found in the bitstream
  bool                            m_APSPresent;                   ///< indicates whether APS has been found in the bitstream
  bool                            m_PHPresent;                    ///< indicates whether PH has been found in the bitstream
public:
  MctsExtApp();
  virtual ~MctsExtApp() {};


  void  create(); ///< create internal members
  void  destroy(); ///< destroy internal members
  void  extract(); ///< main extracting function

protected:
  void  xCreateMctsExtLib(); ///< create internal classes
  void  xDestroyMctsExtLib(); ///< destroy internal classes
  void  xInitMctsExtLib(); ///< initialize decoder class

  
  bool  xIsNaluWithinMCTSSet(int MCTS_id); ///< check whether given Nalu belongs to MCTS SET
  void  xInputToOutputSliceNaluConversion(InputNALUnit &inNalu, OutputNALUnit &outNalu, Slice* curSlice); ///< input to output nalu conversion
  void  xWriteOutput(std::ostream& bitstreamFile, const AccessUnit& accessUnit); ///< write AU into output bitstream
};

//! \}

//#endif
#endif