/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2017, ITU/ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

 /** \file     TAppMctsExtTop.cpp
  \brief    MCTS Extractor application class
  */

#include <list>
#include <vector>
#include <stdio.h>
#include <fcntl.h>
#include <assert.h>
#include <fstream>

#include "MctsExtApp.h"
#include "AnnexBread.h"
#include "NALread.h"
#include "NALwrite.h"
#include "SEI.h"

//#if MCTS_EXTRACTION
  //! \ingroup TAppMctsExt
  //! \{

  // ====================================================================================================================
  // Constructor / destructor / initialization / destroy
  // ====================================================================================================================

MctsExtApp::MctsExtApp()
{
}


void MctsExtApp::create()
{
  m_mctsExtractionInfoPresent = false;
  m_VPSPresent = false;
  m_SPSPresent = false;
  m_PPSPresent = false;
  m_APSPresent = false;
  m_PHPresent = false;
}

void MctsExtApp::destroy()
{
  m_inputBitstreamFileName.clear();
  m_outputBitstreamFileName.clear();
}

// ====================================================================================================================
// Public member functions
// ====================================================================================================================

/**
 - create internal class
 - initialize internal class
 - until the end of the bitstream, call extraction function in TDecMctsExt class
 - delete allocated buffers
 - destroy internal class
 .
 */
void MctsExtApp::extract()
{
  ifstream bitstreamFile(m_inputBitstreamFileName.c_str(), ifstream::in | ifstream::binary);
  if (!bitstreamFile)
  {
    fprintf(stderr, "\nfailed to open input bitstream file `%s' for reading\n", m_inputBitstreamFileName.c_str());
    exit(EXIT_FAILURE);
  }

  InputByteStream bytestream(bitstreamFile);

  // create & initialize internal classes
  xCreateMctsExtLib();
  xInitMctsExtLib();

  int iSkipFrame = 0;
  int iPOCLastDisplay = -MAX_INT;
  //int                 poc;
  //PicList* pcListPic = NULL;
  //VPS *vps = new VPS();
  SPS *sps = new SPS();
  PPS *pps = new PPS();
  APS *aps = NULL;
  PicHeader *picHeader = new PicHeader();

  uint32_t PHTemporalId = 0;

  fstream bitstreamFileOut(m_outputBitstreamFileName.c_str(), fstream::binary | fstream::out);
  if (!bitstreamFileOut)
  {
    fprintf(stderr, "\nfailed to open output bitstream file `%s' for writing\n", m_outputBitstreamFileName.c_str());
    exit(EXIT_FAILURE);
  }

  AccessUnit outAccessUnit;
  while (!!bitstreamFile)
  {
    streampos location = bitstreamFile.tellg();
    AnnexBStats stats = AnnexBStats();
    InputNALUnit inNalu;
    inNalu.m_nalUnitType = NAL_UNIT_INVALID;

    // find next NAL unit in stream
    byteStreamNALUnit(bytestream, inNalu.getBitstream().getFifo(), stats);

    bool bNewPicture = false;
    if (inNalu.getBitstream().getFifo().empty())
    {
      /* this can happen if the following occur:
       *  - empty input file
       *  - two back-to-back start_code_prefixes
       *  - start_code_prefix immediately followed by EOF
       */
      msg(ERROR, "Warning: Attempt to decode an empty NAL unit\n");
    }
    else
    {
      // read NAL unit header
      read(inNalu);
      m_Slice = m_DecLib.getApcSlicePilot();
      // decode HLS, skipping cabac decoding and reconstruction
      bNewPicture = m_DecLib.decode(inNalu, iSkipFrame, iPOCLastDisplay, true);
      
      if ((inNalu.m_nalUnitType == NAL_UNIT_CODED_SLICE_TRAIL) || (inNalu.m_nalUnitType == NAL_UNIT_CODED_SLICE_STSA) || (inNalu.m_nalUnitType == NAL_UNIT_CODED_SLICE_RASL) || (inNalu.m_nalUnitType == NAL_UNIT_CODED_SLICE_RADL) || (inNalu.m_nalUnitType == NAL_UNIT_CODED_SLICE_IDR_W_RADL) || (inNalu.m_nalUnitType == NAL_UNIT_CODED_SLICE_IDR_N_LP) || (inNalu.m_nalUnitType == NAL_UNIT_CODED_SLICE_CRA) || (inNalu.m_nalUnitType == NAL_UNIT_CODED_SLICE_GDR))
      {
        if (m_DecLib.isSliceNaluFirstInAU(true, inNalu))
        {
          m_DecLib.resetAccessUnitNals();
          m_DecLib.resetAccessUnitApsNals();
        }
        m_numNALSlice++;
      }

      if (bNewPicture)
      {
        bitstreamFile.clear();
        bitstreamFile.seekg(location - streamoff(3));
        bytestream.reset();
      }
    }

    if ((bNewPicture || !bitstreamFile || inNalu.m_nalUnitType == NAL_UNIT_EOS) &&
      !m_DecLib.getFirstSliceInSequence())
    {
      m_DecLib.getPcPic()->reconstructed = true;
      if (bitstreamFile || inNalu.m_nalUnitType == NAL_UNIT_EOS)
      {
        m_DecLib.setFirstSliceInPicture(true);
      }
    }
    
    if (inNalu.m_nalUnitType == NAL_UNIT_VPS)
    {
      //vps.copyVPS(m_DecLib.getVPS());
      m_VPSPresent = true;
      continue;
    }
    else if (inNalu.m_nalUnitType == NAL_UNIT_SPS)
    {
      sps->copySPS(m_DecLib.getParameterSetManager().getFirstSPS());
      m_SPSPresent = true;
      continue;
    }
    else if (inNalu.m_nalUnitType == NAL_UNIT_PPS)
    {
      pps->copyPPS(m_DecLib.getParameterSetManager().getFirstPPS());
      m_numTileCols = pps->getNumTileColumns();
      m_numTileRows = pps->getNumTileRows();
      m_tileRowHeight = pps->getTileRowHeightByValue();
      m_tileColWidth = pps->getTileColumnWidthByValue();
      m_picHeightInCtu = pps->getPicHeightInCtu();
      m_picWidthInCtu = pps->getPicWidthInCtu();
      m_CtuSize = pps->getCtuSize();
      m_PPSPresent = true;
      continue;
    }

    else if (inNalu.m_nalUnitType == NAL_UNIT_PREFIX_APS)
    {
      aps = m_DecLib.getParameterSetManager().getFirstAPS();
      m_APSPresent = true;
      continue;
    }

    else if (inNalu.m_nalUnitType == NAL_UNIT_PH)
    {
      picHeader = m_DecLib.getPicHeader();
      PHTemporalId = inNalu.m_temporalId;
      m_PHPresent = true;
      continue;
    }

    else if (!bNewPicture && inNalu.isSlice() && xIsNaluWithinMCTSSet(m_targetMctsIdx))
    {
      fprintf(stdout, "Output bitstream resolution: %dx%d\n\n", m_tileWidth, m_tileHeight);
      m_mctsExtractionInfoPresent = true;
      m_numNALSlice = 0;
      //rewrite input slice 

      // setup
      uint32_t numCtusInOutputPictures = m_DecLib.getPcPic()->slices[m_targetMctsIdx]->getNumCtuInSlice();
      uint32_t numCtusInInputPictures = m_DecLib.getPcPic()->m_ctuNums;

      m_DecLib.getPcPic()->m_ctuNums = numCtusInOutputPictures;
      m_Slice->setPic(m_DecLib.getPcPic());
      m_Slice->resetCtuAddrInSliceMap(m_tileWidth, m_tileHeight, m_CtuSize);
      m_Slice->setNumTilesInSlice(1);

      //modify vps, sps, pps and rewrite
      if (m_VPSPresent)
      {
        /*OutputNALUnit vpsout(NAL_UNIT_VPS);
        m_HLSWriter.setBitstream(&vpsout.m_Bitstream);
        m_HLSWriter.codeVPS(&vps);
        outAccessUnit.push_back(new NALUnitEBSP(vpsout));*/

        m_VPSPresent = false;
      }
      if (m_SPSPresent)
      {
        sps->setMaxPicHeightInLumaSamples(m_tileHeight);
        sps->setMaxPicWidthInLumaSamples(m_tileWidth);

        OutputNALUnit spsout(NAL_UNIT_SPS);
        m_HLSWriter.setBitstream(&spsout.m_Bitstream);
        m_HLSWriter.codeSPS(sps);
        outAccessUnit.push_back(new NALUnitEBSP(spsout));

        m_SPSPresent = false;
      }
      if (m_PPSPresent)
      {
        pps->setNoPicPartitionFlag(true);
        pps->resetTileSliceInfo();
        pps->setPicHeightInLumaSamples(m_tileHeight);
        pps->setPicWidthInLumaSamples(m_tileWidth);
        pps->setLog2CtuSize(ceilLog2(sps->getCTUSize()));
        pps->setNumExpTileColumns(1);
        pps->setNumExpTileRows(1);
        pps->setNumTileColumns(1);
        pps->setNumTileRows(1);
        pps->addTileColumnWidth(pps->getPicWidthInCtu());
        pps->addTileRowHeight(pps->getPicHeightInCtu());
        pps->initTiles();
        pps->setRectSliceFlag(1);
        pps->setNumSlicesInPic(1);
        pps->initRectSlices();
        pps->setTileIdxDeltaPresentFlag(0);
        pps->setSliceTileIdx(0, 0);
        pps->initRectSliceMap();
        //
        //pps->setTemporalId(inNalu.m_temporalId);

        OutputNALUnit ppsout(NAL_UNIT_PPS);
        m_HLSWriter.setBitstream(&ppsout.m_Bitstream);
        m_HLSWriter.codePPS(pps, sps);
        outAccessUnit.push_back(new NALUnitEBSP(ppsout));

        m_PPSPresent = false;
      }
      
      if (m_APSPresent)
      {
        OutputNALUnit apsout(NAL_UNIT_PREFIX_APS);
        m_HLSWriter.setBitstream(&apsout.m_Bitstream);
        m_HLSWriter.codeAPS(aps);
        outAccessUnit.push_back(new NALUnitEBSP(apsout));

        m_APSPresent = false;
      }

      if (m_PHPresent)
      {
        OutputNALUnit phout(NAL_UNIT_PH);
        m_HLSWriter.setBitstream(&phout.m_Bitstream);
        phout.m_temporalId = PHTemporalId;
        picHeader->setPic(m_DecLib.getPcPic());
        m_HLSWriter.codePictureHeader(picHeader);
        outAccessUnit.push_back(new NALUnitEBSP(phout));

        m_PHPresent = false;
      }
      m_Slice->setPicHeader(picHeader);
      m_Slice->setSPS(sps);
      m_Slice->setPPS(pps);
      //m_Slice->setTLayer(inNalu.m_temporalId);
      OutputNALUnit outNalu(m_Slice->getNalUnitType(), 0, m_Slice->getTLayer());
      m_HLSWriter.setBitstream(&outNalu.m_Bitstream);
      m_HLSWriter.codeSliceHeader(m_Slice);
      
      m_Slice->setFinalized(true);

      m_Slice->clearSubstreamSizes();

      m_HLSWriter.setBitstream(&outNalu.m_Bitstream);
      m_HLSWriter.codeTilesWPPEntryPoint(m_Slice);
      
      //convert
      xInputToOutputSliceNaluConversion(inNalu, outNalu, m_Slice);
      //write to file
      outAccessUnit.push_back(new NALUnitEBSP(outNalu));
      xWriteOutput(bitstreamFileOut, outAccessUnit);
      outAccessUnit.clear();

      // console output
      char c = (m_Slice->isIntra() ? 'I' : m_Slice->isInterP() ? 'P' : 'B');
      //if (!m_Slice->isReferenced())
      if (m_Slice->getPicHeader()->getNonReferencePictureFlag())
      {
        c += 32;
      }
      printf("POC %4d TId: %1d ( %c-SLICE, QP%3d ) ", m_Slice->getPOC(),
        m_Slice->getTLayer(),
        c,
        m_Slice->getSliceQp());
      printf(" %10d bits\n", (int)inNalu.getBitstream().getFifo().size());

      m_DecLib.getPcPic()->m_ctuNums = numCtusInInputPictures;

      // Added by jbjeong.
      m_DecLib.getPcPic()->destroyTempBuffers();
      //m_DecLib.getPcPic()->cs->destroyCoeffs();
      //m_DecLib.getPcPic()->cs->releaseIntermediateData();
      //m_DecLib.getPcPic()->cs->picHeader->initPicHeader();
    }
  }

  if (!m_mctsExtractionInfoPresent)
  {
    fprintf(stderr, "\nInput bitstream file `%s' does not contain MCTS extraction information for target MCTS index %d\n", m_inputBitstreamFileName.c_str(), m_targetMctsIdx);
  }

  // delete buffers
  m_DecLib.deletePicBuffer();

  // destroy internal classes
  xDestroyMctsExtLib();

}

// ====================================================================================================================
// Protected member functions
// ====================================================================================================================


void MctsExtApp::xCreateMctsExtLib()
{
  // create decoder class
  m_DecLib.create();
}

void MctsExtApp::xDestroyMctsExtLib()
{
  // destroy decoder class
  m_DecLib.destroy();
}

void MctsExtApp::xInitMctsExtLib()
{
  // initialize decoder class
  m_DecLib.init();
  //m_DecLib.setDecodedPictureHashSEIEnabled(1);
  //m_DecLib.initScalingList();
}

bool MctsExtApp::xIsNaluWithinMCTSSet(int mcts_id)
{
  //uint32_t currNaluTSSliceSegAddr = m_Slice->getSliceSegmentCurStartCtuTsAddr();
  uint32_t currNaluFirstCtuAddr = m_Slice->getFirstCtuRsAddrInSlice();
  uint32_t targetTileFirstCtuAddr = 0;

  uint32_t targetTileIdxInCol = m_targetMctsIdx % m_numTileCols;
  uint32_t targetTileIdxInRow = m_targetMctsIdx / m_numTileCols;
  
  m_tileWidth = m_tileColWidth[targetTileIdxInCol] * m_CtuSize;
  m_tileHeight = m_tileRowHeight[targetTileIdxInRow] * m_CtuSize;

  for (uint32_t col = 0; col < targetTileIdxInCol; col++)
  {
    targetTileFirstCtuAddr += m_tileColWidth[col];
  }

  for (uint32_t row = 0; row < targetTileIdxInRow; row++)
  {
    targetTileFirstCtuAddr += m_tileRowHeight[row] * m_Slice->getPPS()->getPicWidthInCtu();
  }

  //uint32_t firstCtuAddrTSInMcts = m_DecLib.getPcPic()->getPicSym()->getCtuRsToTsAddrMap(m_cTDecTop.getPcPic()->getPicSym()->getTComTile(m_targetMctsIdx)->getFirstCtuRsAddr());
  //uint32_t lastCtuAddrTSInMcts = -1;

  //if (mcts_id < (m_cTDecTop.getPcPic()->getPicSym()->getNumTiles() - 1))
  if (mcts_id < (m_DecLib.getPcPic()->slices[m_numNALSlice - 1]->getPPS()->getNumTiles() - 1))
  {// not last tile
    //lastCtuAddrTSInMcts = m_cTDecTop.getPcPic()->getPicSym()->getCtuRsToTsAddrMap(m_cTDecTop.getPcPic()->getPicSym()->getTComTile(m_targetMctsIdx + 1)->getFirstCtuRsAddr()) - 1;
    //lastCtuAddrTSInMcts = m_DecLib.getPcPic()->slices[m_targetMctsIdx + 1]->getFirstCtuRsAddrInSlice() - 1;
    // My assumption is that the last CTU address in in the last element of ctuAddrList.
    //lastCtuAddrTSInMcts = m_DecLib.getPcPic()->slices[m_numNALSlice - 1]->getCtuAddrInSlice(m_DecLib.getPcPic()->slices[m_numNALSlice - 1]->getNumCtuInSlice() - 1) - 1;
  }
  else
  {
    //lastCtuAddrTSInMcts = m_cTDecTop.getPcPic()->getNumberOfCtusInFrame() - 1;
    //lastCtuAddrTSInMcts = m_DecLib.getPcPic()->m_ctuNums - 1;
  }
  //fprintf(stdout, "currNaluTSSliceSegAddr:%d, targetCtuAddrTSInMcts: %d\n", currNaluFirstCtuAddr, targetTileFirstCtuAddr);
  return (currNaluFirstCtuAddr == targetTileFirstCtuAddr);
}

void MctsExtApp::xInputToOutputSliceNaluConversion(InputNALUnit &inNalu, OutputNALUnit &outNalu, Slice* curSlice)
{
  const uint32_t uiNumSubstreams = curSlice->getNumberOfSubstreamSizes() + 1;
  
  InputBitstream **ppcSubstreams = NULL;
  ppcSubstreams = new InputBitstream*[uiNumSubstreams];
  for (uint32_t ui = 0; ui < uiNumSubstreams; ui++)
  {
    ppcSubstreams[ui] = inNalu.getBitstream().extractSubstream(ui + 1 < uiNumSubstreams ? (curSlice->getSubstreamSize(ui) << 3) : inNalu.getBitstream().getNumBitsLeft());
  }

  OutputBitstream  *pcBitstreamRedirect;
  pcBitstreamRedirect = new OutputBitstream;

  // Append substreams...
  OutputBitstream *pcOut = pcBitstreamRedirect;
  std::vector<OutputBitstream> substreamsOut(uiNumSubstreams);

  for (uint32_t ui = 0; ui < uiNumSubstreams; ui++)
  {
    std::vector<uint8_t> &bufIn = ppcSubstreams[ui]->getFifo();
    std::vector<uint8_t> &bufOut = substreamsOut[ui].getFIFO();
    bufOut.resize(bufIn.size());
    bufOut = bufIn;
    pcOut->addSubstream(&(substreamsOut[ui]));
  }

  // Byte-align
  outNalu.m_Bitstream.writeByteAlignment();

  // Perform bitstream concatenation
  if (pcOut->getNumberOfWrittenBits() > 0)
  {
    outNalu.m_Bitstream.addSubstream(pcOut);
  }
  //pcOut->clear();

  // clean-up
  for (uint32_t ui = 0; ui < uiNumSubstreams; ui++)
  {
    delete ppcSubstreams[ui];
  }
  delete[] ppcSubstreams;
  delete pcBitstreamRedirect;

  return;

}

void MctsExtApp::xWriteOutput(std::ostream& bitstreamFile, const AccessUnit& accessUnit)
{
  for (AccessUnit::const_iterator it = accessUnit.begin(); it != accessUnit.end(); it++)
  {
    const NALUnitEBSP& nalu = **it;

    static const unsigned char start_code_prefix[] = { 0,0,0,1 };
    if (it == accessUnit.begin() || nalu.m_nalUnitType == NAL_UNIT_DPS || nalu.m_nalUnitType == NAL_UNIT_VPS || nalu.m_nalUnitType == NAL_UNIT_SPS || nalu.m_nalUnitType == NAL_UNIT_PPS)
    {
      /* From AVC, When any of the following conditions are fulfilled, the
      * zero_byte syntax element shall be present:
      *  - the nal_unit_type within the nal_unit() is equal to 7 (sequence
      *    parameter set) or 8 (picture parameter set),
      *  - the byte stream NAL unit syntax structure contains the first NAL
      *    unit of an access unit in decoding order, as specified by subclause
      *    7.4.1.2.3.
      */
      bitstreamFile.write(reinterpret_cast<const char*>(start_code_prefix), 4);
    }
    else
    {
      bitstreamFile.write(reinterpret_cast<const char*>(start_code_prefix + 1), 3);
    }
    bitstreamFile << nalu.m_nalUnitData.str();

  }
  /*if (accessUnit.size() > 0)
  {
    bitstreamFile.flush();
  }*/
}
//#endif
